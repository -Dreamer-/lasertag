$(function () {
    $('.phone-input').focus(function() {
        $('.phone-input').mask('+0 (000) 000-0000');
    });

    $('.date-input').mask('00.00.0000', {'translation': {0: {pattern: /[0-9*]/}}});

    $('.nik-input').blur(function() {
        $.post('checkNik__', {nik:$('.nik-input').val()}, function(data) {
            $('.result').css('display', 'inline').html(data);
        })
    });

    $('.pass-input').mask('00-00-00', {'translation': {0: {pattern: /[0-9*]/}}});
});


function edit_status(id) {
    $.ajax({
        type: "post",
        url: "/admin/card/"+id,
        data: {status:$('.status_select').val()},
        success: function(data) {
            $('.title_card').html(data);
        }
    });

    /*$.post('/admin/card/'+id, {status:$('.status_select').val()}, function(data) {
        $('.title_card').html(data);
    })*/
}


/*<script>
 $('a').each(function() {
 var a = new RegExp('/' + window.location.host + '/');
 if(!a.test(this.href)) {
 $(this).click(function(event) {
 event.preventDefault();
 event.stopPropagation();
 window.open(this.href, '_blank');
 });
 }
 });
 </script>*/