<?php
/**
 * Created by PhpStorm.
 * User: dreamer
 * Date: 18.05.15
 * Time: 15:36
 */

class MY_Form_validation extends CI_Form_validation {
    public function alpha_rus($str)
    {
        return ( ! preg_match("/^([а-яa-z])+$/iu", $str)) ? FALSE : TRUE;
    }

    public function alpha_numeric_($str) {
        return ( ! preg_match('/^([\.a-z0-9_-])+$/i', $str)) ? FALSE : TRUE;
    }
}