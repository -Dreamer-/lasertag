<div class='card'>
    <div class='title'>LASERCARD ID</div>
    <div class='photo'><img src="<?= $member->photo?>"></div>
    <div class='data'>
        <div class='nik'><?=$member->nik?></div>
        <div class='data-row'><?=$member->name?> </div>
        <div class='data-row'><?=$member->surname?> </div>
        <div class='data-row'><?=$member->date?></div>
        <div class='num'><?=$member->id?></div>
        <div class='site'>www.golasertag.ru</div>
    </div>
</div>