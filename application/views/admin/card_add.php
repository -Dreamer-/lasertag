<div id="main">
    <div class="full_w">
        <div class="goods_edit">
            <div class="h_title">Добавление карты</div>
            <?php if(validation_errors()): ?>
                <div class="n_error"><p><?= validation_errors(); ?></p></div>
            <?php elseif(!empty($warning)): ?>
                <div class="n_error"><p><?= $warning; ?></p></div>
            <?php endif; ?>
            <form action="" method="post" enctype="multipart/form-data" onsubmit="return checkForm()">
                <div class="element">
                    <label for="title">ID</label>
                    <input id="title" name="id" class="text" value="<?= set_value('id'); ?>" />
                </div>
                <div class="element">
                    <label for="title">Ник</label>
                    <input id="title" name="nik" class="text" value="<?= set_value('nik'); ?>" />
                </div>
                <div class="element">
                    <label for="title">Имя</label>
                    <input id="title" name="name" class="text" value="<?= set_value('name'); ?>" />
                </div>
                <div class="element">
                    <label for="title">Фамилия</label>
                    <input id="title" name="surname" class="text" value="<?= set_value('surname'); ?>" />
                </div>
                <div class="element">
                    <label for="title">Дата рождения</label>
                    <input id="title" name="date" class="text date-input" value="<?= set_value('date'); ?>" />
                </div>
                <div class="element">
                    <label for="title">Телефон</label>
                    <input id="title" name="phone" class="text phone-input" value="<?= set_value('phone'); ?>" />
                </div>
                <div class="element">
                    <label for="title">Электронная почта</label>
                    <input id="title" name="email" class="text" value="<?= set_value('email'); ?>" />
                </div>

                <div class="element input-block preview-block">
                    <!-- hidden crop params -->
                    <input type="hidden" id="x1" name="x1">
                    <input type="hidden" id="y1" name="y1">
                    <input type="hidden" id="x2" name="x2">
                    <input type="hidden" id="y2" name="y2">

                    <label for="title"> Загрузите фотографию</label>

                    <input type="file" multiple name="image_file" id="image_file" onchange="fileSelectHandler()">
                    <div class="canceled_crop"></div>
                    <div class="error_crop" style="text-align: center">
                        <?= form_error('email'); ?>
                    </div>

                    <div class="step2">
                        <center><img id="preview"></center>

                        <div class="info">
                            <input type="hidden" id="filesize" name="filesize">
                            <input type="hidden" id="filetype" name="filetype">
                            <input type="hidden" id="filedim" name="filedim">
                            <input type="hidden" id="w" name="w">
                            <input type="hidden" id="h" name="h">
                        </div>
                    </div>
                </div>

                <div class="element">
                    <button type="submit" class="ok">Save page</button> <!--<button class="cancel" href="/admin/pages">Cancel</button>-->
                </div>
            </form>
        </div>
    </div>
</div>