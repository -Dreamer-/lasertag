<div id="main">
    <div class="full_w">
        <div class="h_title">Список карт</div>
        <div style="display: block; margin: 15px">
            <a class="button add" href="/admin/add_card">Добавить карту</a>
        </div>

        <?php if(!empty($cards)): ?>
        <table>
            <thead>
            <tr>
                <th scope="col" style="width: 7px;">ID</th>
                <th scope="col" style="width: 20px;">NIK</th>
                <th scope="col" style="width: 20px;">Date creation</th>
                <th scope="col" style="width: 10px;">Статус</th>
                <th scope="col" style="width: 7px;">Действия</th>
            </tr>
            </thead>

            <tbody>
            <?php foreach($cards as $j): ?>
                <?php if ($j->status == 1) $j->status = 'Новый'; elseif ($j->status == 2) $j->status = 'В процессе'; elseif ($j->status == 3) $j->status = 'Выполнен'; ?>
                <tr>
                    <td class="align-center"><a href="/admin/card/<?= $j->id; ?>"><?= $j->id;?></a></td>
                    <td><a href="/admin/card/<?= $j->id; ?>"><?= $j->nik;?></a></td>
                    <td class="align-center"><a href="/admin/card/<?= $j->id; ?>"><?= $j->create_date;?></a></td>
                    <td class="align-center"><a href="/admin/card/<?= $j->id; ?>"><?= $j->status;?></a></td>
                    <td class="align-center">
                        <a href="/admin/card_delete/<?= $j->id;?>" class="table-icon delete" title="Удалить"></a>
                    </td>
                </tr>
            <?php endforeach ?>
            <?php else: echo "<div class='n_warning'><p><b>Здесь ничего нет</b></p></div>";?>
            <?php endif;?>
            </tr>
            </tbody>
        </table>
        <?php if(!empty($pagination)): ?>
            <center>
                <div class="sep" style="width: 648px"></div>
            </center>
            <?= $pagination; ?>
        <?php endif; ?>
    </div>
</div>