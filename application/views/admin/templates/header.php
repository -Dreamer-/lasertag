<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pl" xml:lang="pl">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="Paweł 'kilab' Balicki - kilab.pl" />
    <title><?php echo $site_title, ' - ', $page_name ?></title>
    <link rel="stylesheet" type="text/css" href="/css/adm_style.css" />
    <link rel="stylesheet" type="text/css" href="/css/navi.css" />
    <script type="text/javascript" src="/js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="/js/jquery.mask.js"></script>
    <script type="text/javascript" src="/js/main.js"></script>
    <script type="text/javascript">
        $(function(){
            /*$(".box").children(".h_title").click( function() { $(this).next("ul").slideToggle(); });*/

        });
    </script>

    <!--Jcrop-->

    <link href="/css/jquery.Jcrop.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/js/jquery.Jcrop.min.js"></script>
    <script type="text/javascript" src="/js/script.js"></script>
</head>
<body>
<div class="wrap" style=" font-size: 14px; font-weight: bold">
    <div id="header">
        <div id="top">
            <div class="left">
                <p style="font-size: 20px">GO!LASERTAG</p>
            </div>
            <div class="right">
                <div class="align-right">
                    <p>Добро пожаловать, <strong><?php echo $username;?></strong> [ <a href="/admin/logout">Выход</a> ]</p>
                </div>
            </div>
        </div>
        <?php if(empty($username)) {echo 'not found';}?>
        <div id="nav">
            <ul>

                <li class="upp"><a href="/admin/home">Главное управление</a>
                    <ul>
                        <li>&#8250; <a href="/admin/cards">Все карты</a></li>
                        <li>&#8250; <a href="/admin/passwords">Все пароли</a></li>
                        <li>&#8250; <a href="/admin/add_card">Добавить карту</a></li>
                    </ul>
                </li>
                <li class="upp"><a href="/admin/settings">Параметры</a></li>
            </ul>
        </div>
    </div>

    <div id="content">
        <div id="sidebar">
            <div class="box">
                <div class="h_title" style="cursor: default;">Главное управление</div>
                <ul id="home">
                    <li class="b2"><a class="icon page" href="/admin/cards">Все карты</a></li>
                    <li class="b1"><a class="icon report" href="/admin/passwords">Все пароли</a></li>
                </ul>
            </div>
            <!--<div class="box">
                <div class="h_title">&#8250; Manage content</div>
                <ul>
                    <li class="b2"><a class="icon report" href="">Reports</a></li>
                    <li class="b2"><a class="icon add_page" href="">Add new page</a></li>
                </ul>
            </div>-->
        </div>
        <div style="min-height: 500px">