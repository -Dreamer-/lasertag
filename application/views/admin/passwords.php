<div id="main">
    <div class="full_w">
        <div class="h_title">Все пароли регистрации</div>
        <div style="display: block; margin: 15px">
            <a class="button" href="/generate_password">Сгенерировать новые пароли</a>
        </div>

        <?php if(!empty($data)): ?>
        <table>
            <thead>
            <tr>
                <th scope="col" style="width: 7px;">ID</th>
                <th scope="col" style="width: 80%;">Значение</th>
                <th scope="col" style="width: 7px;">Действия</th>
            </tr>
            </thead>

            <tbody class="list-pass">
                <?php foreach($data as $j): ?>
                    <tr>
                        <td class="align-center"><?= $j->id;?></td>
                        <td class="align-center"><?= $j->pass;?></td>
                        <td class="align-center">
                            <a href="/admin/pass_delete/<?= $j->id;?>" class="table-icon delete" title="Delete"></a>
                        </td>
                    </tr>
                <?php endforeach ?>
                <?php else: echo "<div class='n_warning'><p><b>Здесь ничего нет</b></p></div>";?>
                <?php endif;?>
            </tbody>
        </table>
        <?php if(!empty($pagination)): ?>
            <center>
                <div class="sep" style="width: 648px"></div>
            </center>
            <?= $pagination; ?>
        <?php endif; ?>
    </div>
</div>