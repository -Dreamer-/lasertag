<div id="main">
    <div class="full_w">
        <div class="order">
            <div class="h_title title_card"><?= $data->nik; ?> [<?=$data->status?>]</div>
            <form>
                <div class="element">
                    <label for="title" style="font-size: 17px">Information:</label>
                    <div class="entry">Nik: <b><?= $data->nik; ?></b></div>
                    <div class="entry">Имя: <b><?= $data->name; ?></b></div>
                    <div class="entry">Фамилия: <b><?= $data->surname; ?></b></div>
                    <div class="entry">Дата рождения: <b><?= $data->date; ?></b></div>
                    <div class="entry">Номер телефона: <b><?= $data->telephone; ?></b></div>
                    <div class="entry">Email: <b><?= $data->email; ?></b></div><br />
                    <div class="entry"><a href="<?= $data->download; ?>" target="_blank">Загрузить карту (PDF)</a></b></div>
                </div>

                <div class="element">
                    <div class="entry">Дата регестрации: <b><?= $data->create_date; ?></b></div>
                    <div class="entry">Статус:
                        <select name="status" class="status_select" onchange="edit_status(<?=$data->id;?>)">
                            <option value="<?= $data->status; ?>"><?= $data->status; ?></option>
                            <option value="1" style="color: #ff0000;">Новый</option>
                            <option value="2" style="color: #0000FF;">В процессе</option>
                            <option value="3" style="color: #00A100;">Выполнен</option>
                        </select>
                    </div>
                </div>
                <div class="element">
                    <a class="button" href="/admin/card_edit/<?= $data->id; ?>">Изминить данные</a>
                    <a class="button cancel" href="/admin/card_delete/<?= $data->id; ?>">Удалить данные</a>
                </div>
            </form>
        </div>
    </div>
</div>