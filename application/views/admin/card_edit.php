<div id="main">
    <div class="full_w">
        <div class="goods_edit">
            <div class="h_title">Изменение данных (ID: <?= $data->id ?>)</div>
            <?php if(validation_errors()): ?>
                <div class="n_error"><?= validation_errors(); ?></div>
            <?php elseif(!empty($warning)): ?>
                <div class="n_error"><?= $warning; ?></div>
            <?php endif; ?>
            <form action="" method="post">
                <div class="element">
                    <label for="title">Ник</label>
                    <input id="title" name="nik" class="text" value="<?= $data->nik ?>" />
                </div>
                <div class="element">
                    <label for="title">Имя</label>
                    <input id="title" name="name" class="text" value="<?= $data->name ?>" />
                </div>
                <div class="element">
                    <label for="title">Фамилия</label>
                    <input id="title" name="surname" class="text" value="<?= $data->surname ?>" />
                </div>
                <div class="element">
                    <label for="title">Дата рождения</label>
                    <input id="title" name="date" class="text date-input" value="<?= $data->date ?>" />
                </div>
                <div class="element">
                    <label for="title">Телефон</label>
                    <input id="title" name="phone" class="text phone-input" value="<?= $data->telephone ?>" />
                </div>
                <div class="element">
                    <label for="title">Электронная почта</label>
                    <input id="title" name="email" class="text" value="<?= $data->email ?>" />
                </div>
                <div class="element">
                    <button type="submit" class="ok">Save page</button> <!--<button class="cancel" href="/admin/pages">Cancel</button>-->
                </div>
            </form>
        </div>
    </div>
</div>