<div id="main">
    <div class="full_w">
        <div class="h_title">Изменение параметров входа</div>
        <?php if(validation_errors()): ?>
            <div class="n_error"><?= validation_errors(); ?></div>
        <?php elseif(!empty($error_pass)): ?>
            <div class="n_error"><p><?= $error_pass; ?></p></div>
        <?php endif; ?>
        <form action="" method="post">
            <div class="element">
                <label for="title">Логин</label>
                <input id="title" name="login" class="text" style="width: 200px" value="<?= $data->login ?>"/>
            </div>
            <div class="element">
                <label for="name">Пароль</label>
                <input type="password" name="pass" style="width: 200px" value="<?= $data->pass ?>"/>
            </div>
            <div class="element">
                <label for="name">Повторите пароль</label>
                <input type="password" name="repeat" style="width: 200px" value="<?= $data->pass ?>"/>
            </div>
            <div class="entry">
                <button type="submit" class="ok">Сохранить</button>
            </div>
        </form>
    </div>
</div>