<div class="welcome_text">Добро пожаловать на сайт любителей лазертага</div>
<div class="welcome_title">GO!LASERTAG</div>
<div class="welcome_description">Для получения вашей карты, пожалуйста, заполните информацию ниже</div>
<form action="" method="post" class="registration" enctype="multipart/form-data" onsubmit="return checkForm()">
    <div class="input-block">
        <span class="error"><?= form_error('name'); ?></span>
        <input type="text" name="name" placeholder="Имя" value="<?= set_value('name'); ?>" class="form-control input-sm">
    </div>

    <div class="input-block">
        <div class="error"><?= form_error('surname'); ?></div>
        <input type="text" name="surname" placeholder="Фамилия" value="<?= set_value('surname'); ?>" class="form-control input-sm">
    </div>

    <div class="input-block">
        <div class="error"><?= form_error('date'); ?></div>
        <input type="text" name="date" placeholder="Дата рождения" value="<?= set_value('date'); ?>" class="form-control input-sm date-input">
    </div>

    <div class="input-block">
        <div class="error">
            <?= form_error('nik'); ?>
            <?php if(!empty($error_nik)) echo $error_nik; ?>
        </div>
        <input type="text" name="nik" placeholder="Придумайте ваш Ник" value="<?= set_value('nik'); ?>" class="form-control input-sm nik-input">
        <div style="display: none" class="result"></div>
    </div>

    <div class="input-block">
        <div class="error"><?= form_error('phone'); ?></div>
        <input type="text" name="phone" placeholder="Телефон" value="<?= set_value('phone'); ?>" class="form-control input-sm phone-input">
    </div>

    <div class="input-block">
        <div class="error"><?= form_error('email'); ?></div>
        <input type="email" name="email" placeholder="E-mail" value="<?= set_value('email'); ?>" class="form-control input-sm">
    </div>

    <div class="input-block" style="width:96%;">
        <div class="error">
            <?= form_error('pass'); ?>
            <?php if(!empty($error_pass)) echo $error_pass; ?>
        </div>
        <input type="text" name="pass" placeholder="Пароль" style="text-align: center" class="form-control input-sm pass-input">
    </div>

    <div class="input-block preview-block">
        <!-- hidden crop params -->
        <input type="hidden" id="x1" name="x1">
        <input type="hidden" id="y1" name="y1">
        <input type="hidden" id="x2" name="x2">
        <input type="hidden" id="y2" name="y2">

        <div class="main-text"> Загрузите фотографию</div>

        <input type="file" multiple name="image_file" id="image_file" onchange="fileSelectHandler()">
        <div class="error_crop" style="text-align: center">
            <?= form_error('email'); ?>
        </div>

        <div class="step2">
            <center><img id="preview"></center>

            <div class="info">
                <input type="hidden" id="filesize" name="filesize">
                <input type="hidden" id="filetype" name="filetype">
                <input type="hidden" id="filedim" name="filedim">
                <input type="hidden" id="w" name="w">
                <input type="hidden" id="h" name="h">
            </div>
        </div>
    </div>

    <button type="submit" class="btn btn-primary button-ok">Оформить карту</button>
</form>