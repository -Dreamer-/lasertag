<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?= $site_title, ' - ', $site_name ?></title>

    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/development.css">

    <script src="/js/jquery-1.7.2.min.js"></script>
    <script src="/js/jquery.mask.js"></script>
    <script src="/js/main.js"></script>
    <script src="/js/bootstrap.min.js"></script>

    <!--Jcrop-->

    <link href="/css/jquery.Jcrop.min.css" rel="stylesheet" type="text/css" />
    <script src="/js/jquery.Jcrop.min.js"></script>
    <script src="/js/script.js"></script>


</head>
<body>
    <div style="text-align: center">
        <img src="/img/go!.png">
    </div>
    <div class="content">