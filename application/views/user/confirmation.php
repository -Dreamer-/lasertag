<div class="confirm_text">Ваша карта будет иметь такой вид.</div>
<?php if(!empty($data)): ?>
    <div class="card">
        <div class="title">LASERCARD ID</div>
        <div class="photo">
            <img src="<?= $data->photo; ?>" style="height: inherit; width: inherit;">
        </div>
        <div class="data">
            <div class="nik"><?=$data->nik?></div>
            <div class="data-row"><?=$data->name?> </div>
            <div class="data-row"><?=$data->surname?> </div>
            <div class="data-row"><?=$data->date?> </div>
            <div class="num"><?=$data->id?></div>
            <div class='site'>www.golasertag.ru</div>
        </div>
    </div>
<?php endif; ?>
<div class="confirm_ask">Подтверждаете?</div>

<form action="" method="post" style="display: inline">
    <input type="hidden" name="confirm" value="yes">
    <button class="btn btn-primary button-confirm yes" style="left: 25px;">Да</button>
</form>
<form action="" method="post" style="display: inline">
    <input type="hidden" name="confirm" value="no">
    <button class="btn btn-default button-confirm confirmation_close" style="float: right; right: 25px;">Нет</button>
</form>