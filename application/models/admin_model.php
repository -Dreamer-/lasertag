<?php
/**
 * Created by PhpStorm.
 * User: dreamer
 * Date: 23.02.16
 * Time: 21:53
 */


class Admin_model extends CI_Model {
    public function __construct() {
        $this->load->database();
    }

    public function check_admin($login, $pass) {
        return $this->db->get_where('admin', array('login' => $login, 'pass' => $pass))->row();
    }

    public function get_cards($limit, $page) {
        return $this->db->limit($limit, $page)->order_by('id', 'desc')->get('member')->result();
    }

    public function count_cards() {
        return $this->db->count_all('member');
    }

    public function get_card($id) {
        return $this->db->get_where('member', array('id' => $id))->row();
    }

    public function edit_status($id, $data) {
        $this->db->update('member', array('status' => $data), array('id' => $id));
    }

    public function add_card($data) {
        $this->db->insert('member', $data);
        return $this->db->insert_id();
    }

    public function edit_card($id, $data) {
        $this->db->update('member', $data, array('id' => $id));
        $this->db->get('member')->row();
    }

    public function delete_card($id) {
        $this->db->delete('member', array('id' => $id));
    }

    public function get_pass($limit, $page) {
        return $this->db->limit($limit, $page)->order_by('id')->get('passwords')->result();
    }

    public function get_array_pass() {
        return $this->db->select('pass')->order_by('id')->get('passwords')->result();
    }

    public function count_pass() {
        return $this->db->count_all('passwords');
    }

    public function get_admin() {
        return $this->db->get_where('admin', array('id' => 1))->row();
    }

    public function edit_admin($data) {
        $this->db->update('admin', $data, array('id' => 1));
    }
}