<?php
/**
 * Created by PhpStorm.
 * User: dreamer
 * Date: 05.02.16
 * Time: 11:50
 */


class Base_model extends CI_Model {
    public function __construct() {
        $this->load->database();
    }

    public function config($name) {
        return $this->db->get_where('config', array('name' => $name))->row();
    }

    public function generate_pass($pass) {
        $this->db->insert('passwords', array('pass' => $pass));
    }

    public function check_pass($data) {
        return $this->db->get_where('passwords', array('pass' => $data))->row();
    }

    public function delete_pass($data) {
        $this->db->delete('passwords', array('id' => $data));
    }

    public function check_nik($data) {
        return $this->db->get_where('member', array('nik' => $data))->row();
    }

    public function add_member($data) {
        $this->db->insert('member', $data);
    }

    public function update_member($id, $data) {
        $this->db->update('member', $data, array('id' => $id));
    }

    public function delete_member($id) {
        $this->db->delete('member', array('id' => $id));
    }

    public function get_member($id) {
        return $this->db->get_where('member', array('id' => $id))->row();
    }

    public function get_last_member() {
        return $this->db->select('id')->limit(1)->order_by('id', 'desc')->get('member')->row();
    }
}