<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "register/index";
$route['404_override'] = '';

#User

$route['reg'] = 'register/index';
$route['confirmation/(:any)/(:any)'] = 'register/confirmation/$1/$2';
$route['checkNik__'] = 'register/check_nik';
$route['generate_pdf/(:any)/(:any)/(:any)'] = 'register/generate_pdf/$1/$2/$3';
$route['generate_pdf/(:any)/(:any)'] = 'register/generate_pdf/$1/$2';
$route['load_ph/(:any)'] = 'load_photo/index/$1';

#Admin

$route['admin'] = 'admin/authorization/index';
$route['admin/home'] = 'admin/cards/index/$1';
$route['admin/logout'] = 'admin/authorization/logout';

$route['admin/cards/(:any)'] = 'admin/cards/index/$1';
$route['admin/card/(:any)'] = 'admin/cards/card/$1';
$route['admin/add_card'] = 'admin/cards/add_card';
$route['admin/card_edit/(:any)'] = 'admin/cards/edit_card/$1';
$route['admin/card_delete/(:any)'] = 'admin/cards/delete_card/$1';

$route['admin/passwords/(:any)'] = 'admin/passwords/index/$1';
$route['admin/pass_delete/(:any)'] = 'admin/passwords/delete/$1';
$route['generate_password'] = 'admin/passwords/generate';

$route['admin/settings'] = 'admin/settings/index';

/* End of file routes.php */
/* Location: ./application/config/routes.php */