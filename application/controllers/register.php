<?php

require_once 'my_controller.php';
class Register extends My_controller {
    public function __construct() {
        parent::__construct();

        $this->load->library('email');
        $this->load->helper('url');

        $email['newline'] = '\r\n';
        $email['wrapchars'] = '10000';
        $email['mailtype'] = 'html';
        $email['wordwrap'] = TRUE;

        $email['protocol'] = 'smtp';
        $email['smtp_host'] = 'scp22.hosting.reg.ru';
        $email['smtp_user'] = 'zakaz@golasertag.ru';
        $email['smtp_pass'] = 'golasertag52';
        $email['smtp_port'] = '587';

        $this->email->initialize($email);

        $this->email->from($this->settings['email'], 'GO!LASERTAG.');
    }

    public function check_nik() {
        $nik = $this->input->post('nik', true);
        if (!empty($nik) and $this->base_model->check_nik($nik) == false) {
            echo '<span class="glyphicon glyphicon-ok" style="color: #008000; display: inline;right: 530px;top: 303px;position: absolute;" aria-hidden="true"></span>';
        } else {
            echo '<span class="glyphicon glyphicon-remove" style="color: #ff0000; display: inline;right: 530px;top: 303px;position: absolute;" aria-hidden="true"></span>';
        }
    }

    public function index() {
        $data = array();

        $this->form_validation->set_rules('name',               'Имя',              'required|alpha_rus|min_length[2]|xss_clean|prep_for_form');
        $this->form_validation->set_rules('surname',            'Фамилия',          'required|alpha_rus|min_length[2]|xss_clean|prep_for_form');
        $this->form_validation->set_rules('date',               'Дата рождения',    'required|xss_clean|prep_for_form');
        $this->form_validation->set_rules('nik',                'NIK',              'required|alpha_numeric_|xss_clean|prep_for_form');
        $this->form_validation->set_rules('phone',              'Телефон',          'required|xss_clean|prep_for_form');
        $this->form_validation->set_rules('email',              'E-mail',           'required|valid_email|xss_clean|prep_for_form');
        $this->form_validation->set_rules('pass',               'Пароль',           'required|max_length[8]|xss_clean|prep_for_form');

        if ($this->form_validation->run() == true) {
            $form = array(
                'nik'           =>      $this->input->post('nik', true),
                'name'          =>      $this->input->post('name', true),
                'surname'       =>      $this->input->post('surname', true),
                'date'          =>      $this->input->post('date', true),
                'telephone'     =>      $this->input->post('phone', true),
                'email'         =>      $this->input->post('email', true)
            );

            $password = str_replace('-', '', $this->input->post('pass', true));
            $check_pass = $this->base_model->check_pass($password);

            if ($check_pass == true and $this->base_model->check_nik($form['nik']) == false) {
                //Записываем в БД
                $this->base_model->add_member($form);
                //Выводим записаные данные
                $member = $this->base_model->get_last_member();
                //Записываем в сессию, с необходимыми данными
                $this->session->set_userdata(array(
                    'photo' => $this->jcrop->generate('upload/img/'.$member->id),
                    'download' => base_url("upload/pdf/".$member->id.".pdf")
                ));
                redirect('/confirmation/'.$member->id.'/'.$check_pass->id);
            } elseif ($check_pass == false) {
                $data['error_pass'] = 'Не верный пароль!';
            } elseif ($this->base_model->check_nik($form['nik']) == true) {
                $data['error_nik'] = 'Такой Ник уже существует!';
            }
        }

        $this->set_title('Регестрация');
        $this->template('register', $data);
    }

    public function confirmation($id = null, $pass_id = null) {
        $this->form_validation->set_rules('confirm', 'Confirm', 'trim|required|alpha_rus|min_length[2]|xss_clean|prep_for_form');

        $user_data = array(
            'photo' => $this->session->userdata('photo'),
            'download' => $this->session->userdata('download')
        );
        $this->base_model->update_member($id, $user_data);

        $data['data'] = $this->base_model->get_member($id);

        if($this->form_validation->run() == true) {
            if ($this->input->post('confirm', true) == 'yes') {
                //Если пользователь подтверждает, то удаляем пароль, который он ввёл
                $this->base_model->delete_pass($pass_id);
                //Отправляем E-mail
                $this->send_to_email($data['data']);
                // Генерируем и сохраняем PDF файл карточки пользователья
                redirect('/generate_pdf/'.$id.'/F');
            } elseif ($this->input->post('confirm', true) == 'no') {
                //В случае отклонения, удаляем записанные данные из БД
                $this->base_model->delete_member($id);
                //Удаляем фотографию загруженную пользователем
                @unlink('upload/img/'.$id.'.jpg'); # для формта jpg
                @unlink('upload/img/'.$id.'.png'); # для формта png

                redirect('/reg');
            }
        }

        $id_to_card = strval($id);
        $data['data']->id = substr('000000', strlen($id_to_card)). $id_to_card;

        $this->set_title('Проверка');
        $this->template('confirmation', $data);
    }

    public function generate_pdf($id = null, $method = 'F', $for = 'user') {
        $member = $this->base_model->get_member($id);
        $id_to_card = strval($id);
        $member->id = substr('000000', strlen($id_to_card)). $id_to_card;

        include("application/libraries/mpdf60/mpdf.php");

        $mpdf = new mPDF('utf-8', array(86, 54)); //задаем формат, отступы и.т.д.
        $mpdf->charset_in = 'utf-8';

        $stylesheet = file_get_contents('css/pdf.css'); //подключаем css
        $mpdf->WriteHTML($stylesheet, 1);

        $mpdf->list_indent_first_level = 0;
        $mpdf->WriteHTML($this->load->view('pdf', array('member' => $member), true), 2); //формируем pdf
        $mpdf->Output('upload/pdf/'.$id.'.pdf', $method); //выводим pdf

        if ($for === 'admin') {
            redirect('/admin/card/'.$member->id);
        } elseif ($method === 'F') {
            redirect('/reg');
        }
    }

    /*public function jCrop($file_path) {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $iWidth = 240;
            $iHeight = 320; // desired image result dimensions
            $iJpgQuality = 90;

            if (!empty($_FILES)) {

                // if no errors and size less than 250kb
                if (!$_FILES['image_file']['error'] && $_FILES['image_file']['size'] < 2000 * 1024) {
                    if (is_uploaded_file($_FILES['image_file']['tmp_name'])) {

                        // new unique filename
                        $sTempFileName = $file_path;

                        // move uploaded file into cache folder
                        move_uploaded_file($_FILES['image_file']['tmp_name'], $sTempFileName);

                        // change file permission to 644
                        @chmod($sTempFileName, 0644);

                        if (file_exists($sTempFileName) && filesize($sTempFileName) > 0) {
                            $aSize = getimagesize($sTempFileName); // try to obtain image info
                            if (!$aSize) {
                                @unlink($sTempFileName);
                                return;
                            }

                            // check for image type
                            switch ($aSize[2]) {
                                case IMAGETYPE_JPEG:
                                    $sExt = '.jpg';

                                    // create a new image from file
                                    $vImg = @imagecreatefromjpeg($sTempFileName);
                                    break;
                                case IMAGETYPE_PNG:
                                    $sExt = '.png';

                                    // create a new image from file
                                    $vImg = @imagecreatefrompng($sTempFileName);
                                    break;
                                default:
                                    @unlink($sTempFileName);
                                    return;
                            }

                            // create a new true color image
                            $vDstImg = @imagecreatetruecolor($iWidth, $iHeight);

                            // copy and resize part of an image with resampling
                            imagecopyresampled($vDstImg, $vImg, 0, 0, (int)$_POST['x1'], (int)$_POST['y1'], $iWidth, $iHeight, (int)$_POST['w'], (int)$_POST['h']);

                            // define a result image filename
                            $sResultFileName = $sTempFileName . $sExt;

                            // output image to file
                            imagejpeg($vDstImg, $sResultFileName, $iJpgQuality);
                            @unlink($sTempFileName);

                            return base_url($sResultFileName);
                        }
                    }
                }
            }
        }
    }*/

    protected function send_to_email($member) {
        #Send to User
        $id_to_card = strval($member->id);
        $member->id = substr('000000', strlen($id_to_card)). $id_to_card;

        $this->email->to($member->email);
        $this->email->subject('Регистрация прошла успешно!');
        $message = 'Здравствуйте '. $member->name .'!<br />'.
            ' <b>На это сообщение отвечать не нужно</b>. <br />Вы его получили, потому что оформили карту на сайте <a href="worldcat.pe.hu">GO!LASERTAG</a>. Напоминаем ваши регистрационные данные:<br /><br />'
        .'ID: <b>'.$member->id.'</b><br />'
        .'NIK: '.$member->nik.'<br />'
        .'Имя: '.$member->name.'<br />'
        .'Фамилия: '.$member->surname.'<br />'
        .'Дата рождения: '.$member->date.'<br />'
        .'Телефон: '.$member->telephone;
        $this->email->message($message);
        $this->email->send();

        #Send to Admin

        $this->email->to('karta@golasertag.ru');
        $this->email->subject('Произведена регистрация карты ID');
        $this->email->message('Клиент под именем <b>'.$member->name.'</b> только что зарегистрировал карту ID. <br /> Его email - '.$member->email.', и номер телефона - "'.$member->telephone.
            '". <br /> Чтобы загрузить его карту в PDF файле, перейдите по <a href="'.$member->download.'">этой</a> ссылке.');
        $this->email->send();
    }
}