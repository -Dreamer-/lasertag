<?php
/**
 * Class My_controller
 * @property base_model base_model
 * @property CI_Loader load
 * @property CI_session session
 * @property CI_Form_validation form_validation
 * @property Jcrop jcrop
*/
class My_controller extends CI_Controller {
    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Europe/Kiev');

        $this->load->model(array(
            'base_model'
        ));
        $this->load->library(array(
            'form_validation',
            'session',
            'jcrop'
        ));
        $this->load->helper(array('url'));

        $site_name = $this->base_model->config('site_name');

        $this->form_validation->set_message('required', 'Это поле не должно быть пустым');
        $this->form_validation->set_message('alpha_rus', 'Это поле должно состоять только из русских букв');
        $this->form_validation->set_message('alpha', 'Это поле должно состоять только из латинских букв');
        $this->form_validation->set_message('numeric_dot', 'Это поле должно иметь форму dd.mm.yyyy');
        $this->form_validation->set_message('min_length', 'Это поле слишком короткое');
        $this->form_validation->set_message('max_length', 'Это поле слишком длинное');

        $this->settings = array(
            'site_name' => $site_name->value,
            'site_title' => '',
            'email' => 'zakaz@golasertag.ru'
        );
    }

    protected function set_title($string) {
        $this->settings['site_title'] = $string;
    }

    protected function template($index, $data) {
        $data = array_merge($this->settings, $data);

        $this->load->view('user/templates/header', $data);
        $this->load->view("user/$index", $data);
        $this->load->view('user/templates/footer', $data);
    }
}