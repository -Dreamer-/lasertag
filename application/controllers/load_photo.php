<?php

require_once 'my_controller.php';
class Load_photo extends My_controller {
    public function index($mode = 'user') {
        if ($mode === 'admin') $this->load->view('admin/load_photo');
        else $this->load->view('user/load_photo');
    }
}