<?php

require_once 'admin_controller.php';
class Settings extends Admin_controller {
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->check_session();
    }

    public function index() {
        $data['data'] = $this->admin_model->get_admin();

        $this->form_validation->set_rules('login', 'Логин', 'trim|required|max_length[15]|xxs_clean|prep_for_form');
        $this->form_validation->set_rules('pass', 'Пароль', 'trim|required|xxs_clean|prep_for_form');
        $this->form_validation->set_rules('repeat', 'Повтрорный пароль', 'trim|required|xxs_clean|prep_for_form');

        if($this->form_validation->run()) {
            $admin = array(
                'login' => $this->input->post('login', true),
                'pass' => $this->input->post('pass', true)
            );
            if ($admin['pass'] === $this->input->post('repeat', true)) {
                $this->admin_model->edit_admin($admin);
                $this->session->set_userdata(array('username' => $admin['login']));
                redirect('/admin');
            } else {
                $data['error_pass'] = 'Пароли не совпадают. Повторите попытку.';
            }
        }

        $this->set_title('Изменение параметров входа');
        $this->template('settings', $data);
    }
}