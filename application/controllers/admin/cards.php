<?php

require_once 'admin_controller.php';
class Cards extends Admin_controller {
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->check_session();
    }

    public function index($page = 0) {
        $config = array(
            'base_url' => base_url().'admin/cards/',
            'total_rows' =>  $this->admin_model->count_cards(),
            'first_link' => 'В начало',
            'last_link' => 'В конец',
            'next_link' => 'Вперед',
            'prev_link' => 'Назад',
            'num_links' => 3,
            'per_page' => 30,

            'full_tag_open' => '<div class="pagination">',
            'full_tag_close' => '</div>',

            'cur_tag_open' => '<span class="active" style="color: #aeaeae">',
            'cur_tag_close' => '</span>',

        );
        $this->pagination->initialize($config);
        $data['cards'] = $this->admin_model->get_cards($config['per_page'], $page);
        $data['pagination'] = $this->pagination->create_links();
        $this->set_title('Cards');
        $this->template('cards', $data);
    }

    protected function status_r($data) {
        switch ($data) {
            case 1 : return 'Новый';
                break;
            case 2 : return 'В процессе';
                break;
            case 3 : return 'Выполнен';
                break;
        }
    }

    public function card($id) {
        $data['data'] = $this->admin_model->get_card($id);
        $data['data']->status = $this->status_r($data['data']->status);

        $this->form_validation->set_rules('status', 'Статус', 'trim|required|xxs_clean|prep_for_form');

        if ($this->input->post('status', true) == true) {
            $new_status = $this->input->post('status', true);
            $this->admin_model->edit_status($id, $new_status);
            echo $data["data"]->nik. ' ['.$this->status_r($new_status).']';
        } else {
            $this->set_title($data['data']->nik);
            $this->template('card', $data);
        }

    }

    public function edit_card($id) {
        $data['data'] = $this->admin_model->get_card($id);

        $this->form_validation->set_rules('nik',                'NIK',              'required|alpha_numeric_|xss_clean|prep_for_form');
        $this->form_validation->set_rules('name',               'Имя',              'required|alpha_rus|min_length[2]|xss_clean|prep_for_form');
        $this->form_validation->set_rules('surname',            'Фамилия',          'required|alpha_rus|min_length[2]|xss_clean|prep_for_form');
        $this->form_validation->set_rules('date',               'Дата рождения',    'required|xss_clean|prep_for_form');
        $this->form_validation->set_rules('phone',              'Телефон',          'required|xss_clean|prep_for_form');
        $this->form_validation->set_rules('email',              'E-mail',           'required|valid_email|xss_clean|prep_for_form');

        if ($this->form_validation->run() == true) {
            $form = array(
                'nik'           =>      $this->input->post('nik', true),
                'name'          =>      $this->input->post('name', true),
                'surname'       =>      $this->input->post('surname', true),
                'date'          =>      $this->input->post('date', true),
                'telephone'     =>      $this->input->post('phone', true),
                'email'         =>      $this->input->post('email', true),
                'status'        =>      2
            );

            if ($this->base_model->check_nik($form['nik']) == false or $form['nik'] === $data['data']->nik) {
                $this->admin_model->edit_card($id, $form);
                redirect('/generate_pdf/'.$data['data']->id.'/F/admin');
            } else {
                $data['warning'] = 'Такой Ник уже существует!';
            }
        }

        $this->set_title('Edit card');
        $this->template('card_edit', $data);
    }

    public function add_card() {
        $data = array();

        $this->form_validation->set_rules('id',                 'ID',               'numeric|xss_clean|prep_for_form');
        $this->form_validation->set_rules('nik',                'NIK',              'required|alpha_numeric_|xss_clean|prep_for_form');
        $this->form_validation->set_rules('name',               'Имя',              'required|alpha_rus|min_length[2]|xss_clean|prep_for_form');
        $this->form_validation->set_rules('surname',            'Фамилия',          'required|alpha_rus|min_length[2]|xss_clean|prep_for_form');
        $this->form_validation->set_rules('date',               'Дата рождения',    'required|xss_clean|prep_for_form');
        $this->form_validation->set_rules('phone',              'Телефон',          'required|xss_clean|prep_for_form');
        $this->form_validation->set_rules('email',              'E-mail',           'required|valid_email|xss_clean|prep_for_form');

        if ($this->form_validation->run() == true) {
            $form = array(
                'nik'           =>      $this->input->post('nik', true),
                'name'          =>      $this->input->post('name', true),
                'surname'       =>      $this->input->post('surname', true),
                'date'          =>      $this->input->post('date', true),
                'telephone'     =>      $this->input->post('phone', true),
                'email'         =>      $this->input->post('email', true),
                'status'        =>      2
            );

            if ($this->base_model->check_nik($form['nik']) == false) {
                $new_id = $this->input->post('id', true);
                if (!empty($new_id) and $this->admin_model->get_card($new_id) == false) {
                    $id = $this->admin_model->add_card($form);
                    $file_data = array(
                        'id' => $new_id,
                        'photo' => $this->jcrop->generate('upload/img/'.$new_id),
                        'download' => base_url("upload/pdf/".$new_id.".pdf")
                    );
                    $this->admin_model->edit_card($id, $file_data);
                    redirect('/generate_pdf/'.$new_id.'/F/admin');
                } elseif ($this->admin_model->get_card($new_id) == true) {
                    $data['warning'] = 'Такой ID уже существует!';
                } elseif (empty($new_id)) {
                    $id = $this->admin_model->add_card($form);
                    $file_data = array(
                        'photo' => $this->jcrop->generate('upload/img/'.$id),
                        'download' => base_url("upload/pdf/".$id.".pdf")
                    );
                    $this->admin_model->edit_card($id, $file_data);
                    redirect('/generate_pdf/'.$id.'/F/admin');
                }
            } else {
                $data['warning'] = 'Такой Ник уже существует!';
            }
        }

        $this->set_title('Add card');
        $this->template('card_add', $data);
    }

    public function delete_card($id) {
        $this->admin_model->delete_card($id);
        @unlink('upload/pdf/'.$id.'.pdf');
        @unlink('upload/img/'.$id.'.jpg');
        @unlink('upload/img/'.$id.'.png');
        redirect('/admin/cards');
    }
}