<?php

require_once 'admin_controller.php';
class Authorization extends Admin_controller {
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
    }

    public function index() {
        $data = array();
        $this->form_validation->set_rules('login', 'Логин', 'trim|required|max_length[15]|xxs_clean|prep_for_form');
        $this->form_validation->set_rules('pass', 'Пароль', 'trim|required|xxs_clean|prep_for_form');
        $session_id = $this->session->userdata('id_admin');

        if($this->form_validation->run() == true) {
            $login = $this->input->post('login');
            $pass = $this->input->post('pass');
            $check = $this->admin_model->check_admin($login, $pass);

            if ($check == true) {
                $this->session->set_userdata(array(
                    'id_admin' => $check->id,
                    'username' => $check->login
                ));
                redirect('/admin/home');
            } else {
                $data['error'] = 'Неверный логин или пароль';
            }
        } elseif (!empty($session_id)) {
            redirect('/admin/home');
        }

        $this->set_title('Панель администрирования');
        $this->load->view('admin/auth', $data);
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect('/admin');
    }
}