<?php

require_once 'admin_controller.php';
class Passwords extends Admin_controller {
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->check_session();
    }

    public function index($page = 0) {
        $config = array(
            'base_url' => base_url().'admin/passwords/',
            'total_rows' =>  $this->admin_model->count_pass(),
            'first_link' => 'В начало',
            'last_link' => 'В конец',
            'next_link' => 'Вперед',
            'prev_link' => 'Назад',
            'num_links' => 3,
            'per_page' => 50,

            'full_tag_open' => '<div class="pagination">',
            'full_tag_close' => '</div>',

            'cur_tag_open' => '<span class="active" style="color: #aeaeae">',
            'cur_tag_close' => '</span>',

        );
        $this->pagination->initialize($config);
        $data['data'] = $this->admin_model->get_pass($config['per_page'], $page);
        $data['pagination'] = $this->pagination->create_links();

        $this->set_title('Пароли');
        $this->template('passwords', $data);
    }

    public function generate() {
        for ($j = 1; $j <= 100; $j++) {
            $count[] = mt_rand(100000, 999999);
        }
        foreach ($this->admin_model->get_array_pass() as $obj_pass) {
            $pass[] = $obj_pass->pass;
        }
        if (!empty($pass)) {
            $array_pass = array_diff($count, $pass);
            foreach ($array_pass as $data) {
                $this->base_model->generate_pass($data);
            }
        } else {
            foreach ($count as $data) {
                $this->base_model->generate_pass($data);
            }
        }

        redirect('/admin/passwords');
    }

    public function delete($id) {
        $this->base_model->delete_pass($id);
        redirect('/admin/passwords');
    }
}