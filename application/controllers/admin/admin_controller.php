<?php
/**
 * Class My_controller
 * @property base_model base_model
 * @property admin_model admin_model
 * @property CI_Loader load
 * @property CI_session session
 * @property CI_Form_validation form_validation
 * @property Jcrop jcrop
 */
class Admin_controller extends CI_Controller {
    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Europe/Kiev');

        $this->load->model(array(
            'base_model',
            'admin_model'
        ));
        $this->load->library(array(
            'form_validation',
            'session',
            'pagination',
            'jcrop'
        ));
        $this->load->helper(array(
            'url'
        ));

        $site_name = $this->base_model->config('site_name');

        $this->form_validation->set_message('required', '%s не должен быть пустым');
        $this->form_validation->set_message('alpha_rus', 'Это поле должно состоять только из русских букв');
        $this->form_validation->set_message('alpha', 'Это поле должно состоять только из латинских букв');
        $this->form_validation->set_message('numeric_dot', 'Это поле должно иметь форму dd.mm.yyyy');
        $this->form_validation->set_message('min_length', 'Это поле слишком короткое');
        $this->form_validation->set_message('max_length', 'Это поле слишком длинное');

        $this->settings = array(
            'site_name' => $site_name->value,
            'page_name' => 'ADMIN',
            'page_title' => '',
        );
    }

    protected function set_title($string) {
        $this->settings['site_title'] = $string;
    }

    protected function check_session() {
        $session_id = $this->session->userdata('id_admin');
        if(empty($session_id)) {redirect('/admin');}
    }

    protected function template($index, $data) {
        $data['username'] = $this->session->userdata('username');
        $data['session_id'] = $this->session->userdata('id_admin');
        $data = array_merge($this->settings, $data);

        $this->load->view('admin/templates/header', $data);
        $this->load->view("admin/$index", $data);
        $this->load->view('admin/templates/footer', $data);
    }
}